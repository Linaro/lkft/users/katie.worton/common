#!/bin/bash

# $1: Build name
#
# Outputs "1" if build should be deemed
# a release, "0" if it's not a release.
is_release() {
  build_name="$1"
  # Stable releases
  if grep -qE '^v[0-9]*\.[0-9]*\.[0-9]*$' <(echo "${build_name}"); then
    echo "1"
  fi

  ## Mainline is currently inactive
  # Mainline releases
  if grep -qE '^v[0-9]*\.[0-9]*$' <(echo "${build_name}"); then
    echo "1"
  fi

  # Mainline release candidates
  if grep -qE '^v[0-9]*\.[0-9]*-rc[0-9]$' <(echo "${build_name}"); then
    echo "1"
  fi
}

export QA_PROJECT="${QA_PROJECT//\//_}"
echo "QA_SERVER: ${QA_SERVER}"
echo "QA_PROJECT: ${QA_PROJECT}"
echo "QA_PROJECT_NAME: ${QA_PROJECT_NAME}"
test -n "${QA_PROJECT_NAME}" || export QA_PROJECT_NAME="${QA_PROJECT}"

SQUAD_PROJECT="${QA_PROJECT=}"

if [ -z "${BUILD_ID}" ]; then
  source lib.sh
  BUILD_ID="$(get_git_describe build.json)"
fi

if [ "$(is_release "${BUILD_ID}")" = "1" ]; then
  # Mark build as release in SQUAD
  build_url="${QA_SERVER}/api/builds?version=${BUILD_ID}&project__slug=${SQUAD_PROJECT}"
  echo "Build URL: ${build_url}"
  curl -f --silent -L "${build_url}" -o qa_build.json
  build_id="$(jq -r '.results[0].id' qa_build.json)"
  curl --silent \
    -X PATCH "${QA_SERVER}/api/builds/${build_id}/" \
    -H "Authorization: Token ${QA_REPORTS_TOKEN}" \
    -H "Content-Type: application/json" \
    -d "{\"is_release\": true, \"release_label\": \"${BUILD_ID}\"}"
fi
