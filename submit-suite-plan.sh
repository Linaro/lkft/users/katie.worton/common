#!/bin/bash

suite_plan="${1:-"master-plan-ltp.yml"}"
name="$(echo "${suite_plan}" | awk -F'-' '{print $NF}' | awk -F'.' '{print $1}')-tuxbuild_url-arm64-gcc:"
curl -sSOL https://gitlab.com/Linaro/lkft/pipelines/lkft-common/-/raw/master/tuxconfig/"${suite_plan}"
download_url=$(grep "^${name}" "${suite_plan}" | awk -F' ' '{print $NF}')
kernel_version=$(curl -sSL "${download_url}"/status.json | jq -r '.git_describe')

if [[ ${suite_plan} = *ltp* ]]; then
  suite="ltp"
fi
suite_version=$(curl -sSL https://storage.tuxboot.com/overlays/debian/trixie/arm64/"${suite}"/master/version.txt)
pip --quiet --no-cache-dir install squad-client
squad_build_version="${kernel_version}_${suite_version}"

#Check if squad_build_version is in squad already.
#If yes exit.
squad-client --squad-token="${QA_REPORTS_TOKEN}" --squad-host="${QA_SERVER}" download-results --group="${QA_TEAM}" --project="${QA_PROJECT}" --build="${squad_build_version}" > /dev/null
retval=$?
if [[ ${retval} == 0 ]]; then
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  echo "Nothing to build test, results for SQUAD build_id: ${squad_build_version} is already in SQUAD"
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  exit 0
fi
#if no submit a tuxsuite plan
# shellcheck disable=SC2086
tuxsuite plan ${suite_plan} --no-wait --json-out build-plan.json --callback ${QA_SERVER}/api/fetchjob/${QA_TEAM}/${QA_PROJECT}/${squad_build_version}/env/tuxsuite.com
