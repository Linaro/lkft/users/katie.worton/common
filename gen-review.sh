#!/bin/bash

set -euxo pipefail

source "lib.sh"

if [ -z "${BUILD_ID}" ]; then
  BUILD_ID="$(get_git_describe build.json)"
fi

suffix="$(get_project_suffix)"

build-email \
        --msg-from="${REVIEW_FROM}" \
        --msg-to="${REVIEW_TO}" \
        --msg-cc="${REVIEW_CC}" \
        --msg-subject="[review] ${QA_TEAM} ${REPORT_TYPE} for ${BUILD_ID}${suffix}" \
        --msg-body="${REPORT_TYPE}.txt" \
        --msg-body-pre="Go here to send the report upstream ${CI_PIPELINE_URL}." > review.txt
