#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2023-present Linaro Limited
#
# SPDX-License-Identifier: MIT

import argparse
import json
import logging
import os
import re
import sys

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

def parse_args():
    parser = argparse.ArgumentParser(description="parse tuxsuite build.json")

    parser.add_argument(
        "--build-name",
        required=True,
        help="build name",
    )

    parser.add_argument(
        "--filename",
        required=True,
        type=argparse.FileType('r'),
        help="Name of the json input file e.g., build-plan.json"
    )

    parser.add_argument(
        "--debug",
        action="store_true",
        default=False,
        help="Display debug messages",
    )

    return parser.parse_args()

def run():
    args = parse_args()
    if args.debug:
        logger.setLevel(level=logging.DEBUG)

    data = json.load(args.filename)
    for build in data['builds'].values():
        logger.debug(f"{build['build_name']}")
        if args.build_name in build['build_name']:
            toolchain = build['toolchain'] if 'nightly' in build['toolchain'] else build['toolchain'].split('-')[0]
            if args.build_name in ['kselftest', 'perf']:
                if "graviton" in build['build_name']:
                    print(f"{args.build_name}-{build['target_arch']}-{toolchain}-graviton {build['download_url']}{args.build_name}.tar.xz")
                else:
                    print(f"{args.build_name}-{build['target_arch']}-{toolchain} {build['download_url']}{args.build_name}.tar.xz")
            elif args.build_name == build['build_name']:
                print(f"tuxbuild_url-{build['target_arch']}-{toolchain} {build['download_url']}")
            elif '-compat' in build['build_name']:
                print(f"tuxbuild_url-{build['target_arch']}-{toolchain}-compat {build['download_url']}")


if __name__ == "__main__":
    sys.exit(run())
