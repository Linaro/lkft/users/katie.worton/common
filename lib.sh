#!/bin/bash

# $1: git repo url
# $2: remote name
# $3: directory
clone_or_update() {
  git_repo="$1"
  remote="$2"
  directory="$3"
  if [ ! -d "${directory}" ]; then
    git clone -o "${remote}" "${git_repo}" "${directory}" > /dev/null
  else
    pushd "${directory}" > /dev/null || exit 1
    if ! git remote -v | grep -q "^${remote}\s"; then
      git remote add "${remote}" "${git_repo}" > /dev/null
    fi
    git fetch "${remote}" > /dev/null 2>&1
    popd > /dev/null || exit 1
  fi
}

# $1: file to modify
# $2: key
# $3: value
replace_line() {
  if ! grep -q "^[[:space:]]*$2" "$1"; then
    # YAML can only be edited in-place, due to context
    return
  fi

  line="$(grep "^[[:space:]]*$2" "$1")"
  whitespace="$(echo "${line}" | grep -Eo "^[[:space:]]*")"

  sed -i -e "s#^${whitespace}${2}.*#${whitespace}${2}${3}#" "$1"
}

# $1: url
# $2: machine
get_bundle_from_tuxpub() {
  curl -sSL https://gitlab.com/Linaro/lkft/rootfs/dir2bundle/-/archive/master/dir2bundle-master.tar.gz | tar zxf - --overwrite

  machine="$2"
  dir_json="$(mktemp)"
  bundle_json="$(mktemp)"

  curl -sSL -o "${dir_json}" "$1/?export=json"

  jq -r '.files[].Url' "${dir_json}" | sed -e "s|$1/||g" | ./dir2bundle-master/dir2bundle "${machine}" > "${bundle_json}"

  cat "${bundle_json}"
}

# $1 parameter to extract
#    This argument is required
# $2 location of bundle.json
#    This is optional. Defaults to
#    bundle.json in the current
#    directory.
get_from_bundle() {
  bundle_json="bundle.json"
  if [ $# -gt 1 ]; then
    bundle_json="$2"
  fi
  jq -r ".$1" "${bundle_json}"
}

# $QA_PROJECT: Slug of the QA project,
#     from which we'll determine if it's
#     a sanity project or not.
#     Environment variable.
get_project_suffix() {
  local suffix=""
  if grep -q sanity <<< "${QA_PROJECT}"; then
    suffix=" - sanity"
  fi

  echo "${suffix}"
}

# $1: name of build.json artifact
get_git_describe() {
  build_json="${1}"

  builds_count=$(jq -r '.builds | length' "${build_json}" 2> /dev/null)

  if [ "${builds_count}" -eq 0 ]; then
    retval=$(jq -r '.git_describe' "${build_json}")
  else
    retval=$(jq -r '.builds[].git_describe' "${build_json}" | head -n1)
  fi

  echo "${retval}"
}
